<img alt="TTconsole" src="logo/terminal.png"/>


TTconsole for TomTom
====================

VERSION 1.14

(c) 2007-2011 by Markus Hoffmann

part of the source code is copyright (c) 1988-2011 Markus Hoffmann 

    Name        : TTconsole                    
    Version     : 1.14
    Group       : Embedded/Shell
    License     : GPL
    Author      : Markus Hoffmann 
    URL         : http://www.opentom.org/TomTom_Console
    Summary     : A Text console for the TomTom ONE car navigation system



Description
-----------

With a virtual keyboard, you can enter shell commands. (To activate the
virtual keyboard, you have to touch the upper right corner of the screen.) 
It is recommended to use a pen instead of your fingers.

The text screen has a size of 64x30 charackters (on the TomTom ONE) if the
keyboard is invisible and 64x25 if the virtual keyboard is shown on the
console. Al though you can hide the keyboard you can still use it (if you
remember where the key are). Also you can cut an paste text regions from the
screen to the input.

TTconsole implements a terminal emulation with the small font on a couloured 
framebuffer device. It then spawns a shell (default /bin/sh). The stdout 
and stderr is piped to TTconsole which displays it (or feeds stdin with the 
keys you clicked on the virtual keyboard or the cut/paste/buffer).


It appears that the builtin shell is busybox. There is no editor (like pico or
nano) installed (except for vi of course) but you can put the pico excecutable
on the INTERNAL directory which appears at /mnt/sdcard/.

Excecution of shell scripts works well.

The Software is not tested on other devices than TomTom ONE V3, 
but it might work.


The framebuffer rendering and Touchscreen routines are based on 
the Hello World Example which was published (GPL) by TomTom.

Some procedures are taken from the X11Basic package.


### Install

Unpack the binary package (zip file) and move the contents to your tomtom
directory (e.g. /media/INTERNAL) just as they are. Thats it. It is not necessary
to rebuild the systems kernel. You do not need opentom linux! You can use the
device with navigation software as before. The icon for TTconsole should appear
in the setting menu.

### Compile

Unpack the tar.gz file and read the README and follow the instructions. You
will need the ARM-Linux crosscompiler development environment.

### ACKNOWLEDGEMENTS

Many thanks to Joghurt for help in adapting the touchscreen routines for
TomTom firmware 8.03.

### Screenshots

<div style="display:flex;">
<img alt="App image" src="screenshots/img_5410.jpg" width="49%">
<img alt="App image" src="screenshots/img_5411-klein.jpg" width="49%">
</div>
<div style="display:flex;">
<img alt="App image" src="screenshots/img_5414-klein.jpg" width="49%">
<img alt="App image" src="screenshots/img_5415-klein.jpg" width="49%">
</div>
<div style="display:flex;">
<img alt="App image" src="screenshots/img_5417-klein.jpg" width="49%">
<img alt="App image" src="screenshots/keyboard-1.06.png" width="49%">
</div>

### Important Note:

    TTconsole is free software and comes with NO WARRANTY - read the file
    COPYING for details
    
(Basically that means, free, open source, use and modify as you like, don't
incorporate it into non-free software, no warranty of any sort, don't blame me
if it doesn't work.)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

